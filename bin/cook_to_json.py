import csv
import json
import sys
import argparse

from util.NeuroMLUtilities import ConnectionInfo

# Input file have to be cvs with first empty columns and rows deleted
# First column must be named as src

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# Muscle prefix new names
def get_all_muscle_prefixes():
    return ["pm", "vm", "um", "dBWM", "vBWM"]

# Is muscle new names
def is_muscle(cell):
    known_muscle_prefixes = get_all_muscle_prefixes()
    return cell.startswith(tuple(known_muscle_prefixes))

def is_neuron(cell):
    #return cell[0].isupper()
    return not is_muscle(cell)

def remove_leading_index_zero(cell):
    """
    Returns neuron name with an index without leading zero. E.g. VB01 -> VB1.
    """
    if is_neuron(cell) and cell[-2:].startswith("0"):
        return "%s%s" % (cell[:-2], cell[-1:])
    return cell

def get_old_cell_name(cell):

    if is_muscle(cell) and len(cell) > 5:
        index = int(cell[5:])
        if index < 10:
            index = "0%s" % index
        if cell.startswith("vBWML"):
            return "MVL%s" % index
        elif cell.startswith("vBWMR"):
            return "MVR%s" % index
        elif cell.startswith("dBWML"):
            return "MDL%s" % index
        elif cell.startswith("dBWMR"):
            return "MDR%s" % index
    else:
        return remove_leading_index_zero(cell)

def get_dupli(ls):
    seen = {}
    dupes = []

    for x in ls:
        if x not in seen:
            seen[x] = 1
        else:
            if seen[x] == 1:
                dupes.append(x)
            seen[x] += 1
    return dupes

def print_row(key, values, end=""):
    print(json.dumps(key)+':', json.dumps(values, sort_keys=True), end=end)
    # print(json.dumps(key)+':', json.dumps(values), end=end)
    tgt_cells_tuples = values.keys()
    dupli = get_dupli(list(tgt_cells_tuples))
    if dupli:
        eprint("Dulicateds in {}: {}".format(key, dupli))

def is_cell(v):
    # Ignore hypodermal cell. Is not neuron nor muscle
    return v != "hyp"


def get_tgt(cell_src, cell_tgt, v, is_chemical=False):

    if is_chemical:
        # Test if inhibitory
        if ((cell_src.startswith("DD") or cell_src.startswith("VD"))
            and is_muscle(cell_tgt)):
            value = -int(v)
        else:
            value = int(v)
    else:
        value = int(v)

    cell_tgt_old_name = get_old_cell_name(cell_tgt)
    return cell_tgt_old_name, value

def csv_ady_to_dict(file, ady_json, is_chemical=False):

    with open(file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            cell_src = row['src']
            if cell_src:
                del row['src']
                del row['']
                cell_src_old_name = get_old_cell_name(cell_src)
                tgts = dict([get_tgt(cell_src, cell_tgt, v, is_chemical=is_chemical)
                        for cell_tgt, v in row.items()
                        if v and is_cell(cell_tgt)])
                # tgts = {cell_tgt_old_name: value
                #         for cell_tgt_old_name, value in get_tgt(cell_src, cell_tgt, v, is_chemical=is_chemical)
                #         for cell_tgt, v in row.items()
                #         if v and is_cell(cell_tgt)}
                if tgts:

                    if cell_src_old_name not in ady_json.keys():
                        ady_json[cell_src_old_name] = tgts
                    else:
                        for c in set(ady_json[cell_src_old_name]) | set(tgts):

                            if c in ady_json[cell_src_old_name].keys(): # and c in tgts.keys():
                                eprint("{} to {} repeated in {}".format(cell_src_old_name, c, file))

                            ady_json[cell_src_old_name][c] = ady_json[cell_src_old_name].get(c, 0) + tgts.get(c, 0)
                          
    return ady_json

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-fc", "--file-chemical", help="Chemical ady matrix")
    parser.add_argument("-fa", "--file-gap-asym", help="Gap asymentrical ady matrix")
    parser.add_argument("-fs", "--file-gap-sym", help="Gap symentrical ady matrix")
    args = parser.parse_args()
 
    ady_json = {}

    if args.file_chemical:
        ady_json = csv_ady_to_dict(args.file_chemical, ady_json, is_chemical=True)
    
    if args.file_gap_asym:
        ady_json = csv_ady_to_dict(args.file_gap_asym, ady_json, is_chemical=False)
    
    if args.file_gap_sym:
        ady_json = csv_ady_to_dict(args.file_gap_sym, ady_json, is_chemical=False)
    
    print("{")
    key_values = sorted(ady_json.items())

    for key, values in key_values[:-1]:
        print_row(key, values)
        print(",")

    print_row(*key_values[-1], end="\n")

    print("}")


if __name__ == '__main__':
    main()
