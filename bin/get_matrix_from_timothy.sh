#!/bin/bash

# set -x
# Have to delete final comma

FILE="$1"

sps='[[:space:]]*'
spsp='[[:space:]]+'

get_name_fun(){
    echo $1 | sed "s/${sps}def${sps}\(\w*\).*/\1/"
}

get_target_neuron(){
    echo "$1" | sed "s/[^']*'\([A-Z0-9]*\)'.*/\1/"
}

get_weight(){
    echo "$1" | sed "s/[^=]*=${sps}\([-0-9]*\).*/\1/"
}

empty_line(){
    line="$1"
    echo "$line" | grep -qE "^${sps}$"
}

is_comment(){
    line="$1"
    echo "$line" | grep -qE "^${sps}#"
}

read_until_not_empty(){

    while read line && empty_line "$line"; do
        :
    done
    echo "$line"
}

is_begin_func(){
    echo "$1" | grep -qE "^${sps}def${spsp}[A-Z0-9]+\(self\):${sps}\$"
}
read_until_activation() {
 
    line="$1"
    # if begin neuron activation
    while ! is_begin_func "$line" && read line; do
        :
    done

    if is_begin_func "$line"; then
        echo "$line"
    else
        exit 1
    fi
}

echo "{"
while read line; do
    line_fun="$(read_until_activation "$line")"
    [ $? == 0 ] || break
    # echo $line_fun
    fun=$(get_name_fun "$line_fun")
    echo -n \"${fun}\": "["
    line=$(read_until_not_empty)

    tgt=$(get_target_neuron "$line") 
    w=$(get_weight "$line")
    echo -n "[\"$tgt\",$w]"
    while read line && ! empty_line "$line"; do
        if is_comment "$line"; then
            continue
        fi
        tgt=$(get_target_neuron "$line") 
        w=$(get_weight "$line")
        echo -n ",[\"$tgt\",$w]"
    done
    echo "],"

done < $FILE
echo "}"
