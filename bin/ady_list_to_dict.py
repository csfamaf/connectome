import json 
import argparse

def print_row(key, values, end=""):
    print(json.dumps(key)+':', json.dumps(values), end=end)

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("json_file_list", help="JSON file with list values")
    args = parser.parse_args()

    ady_json_list = {}

    # Opening JSON file 
    with open(args.json_file_list) as json_file:
        ady_json_list = json.load(json_file)

    ady_json_dict = {}
    for k in ady_json_list.keys():
        ls = ady_json_list[k]
        ady_json_dict[k] = {}
        for n, v in ls:
            ady_json_dict[k][n] = ady_json_dict[k].get(n,0) + v

    print("{")
    key_values = list(ady_json_dict.items())

    for key, values in key_values[:-1]:
        print_row(key, values)
        print(",")

    print_row(*key_values[-1], end="\n")

    print("}")


if __name__ == '__main__':
    main()
