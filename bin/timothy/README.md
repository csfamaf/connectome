## Flujo del programa

ar = al = 0 : sp=0 (stop)
ar <=0  /\  al < 0   /\  tr <=0.6: sp=150 (Marcha atras izq) 
ar <=0  /\  al < 0   /\  tr >=2.0: sp=150 (Marcha atras der) 
ar <=0  /\  al < 0   /\  !tr          : sp=150 (Marcha atras) 
ar <=0  /\  al >= 0                   : sp=newspeed (Rota der) 
ar >=0  /\  al <= 0                   : sp=newspeed (Rota izq) 
ar >=0  /\  al > 0   /\  tr <=0.6: sp=newspeed (Marcha adelante izq) 
ar >=0  /\  al > 0   /\  tr >=2.0: sp=newspeed (Marcha adelante der) 
ar >=0  /\  al > 0   /\ !tr          : sp=newspeed (Marcha adelante) 
al/r es accumleft/righttr es 
turnratiosp es la velocidad (raro es que clava a 150 solo en marcha atras)
newspeed se calcula segun la suma de abs de al y a ar
