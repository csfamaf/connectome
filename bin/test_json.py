import json 
import argparse

def print_row(key, values, end=""):
    vs = [[c, values[c]] for c in sorted(values.keys())]
    print(json.dumps(key)+':', json.dumps(vs), end=end)

def print_row_if_inh(key, values, end=""):
    values_neg = {n: v for n, v in values.items() if v < 0}
    if values_neg:
        print_row(key, values_neg, end=end)

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("json_file", help="JSON file")
    parser.add_argument("-fi", "--filter-inhibitory", action="store_true", help="Show only inhibitory conections")
    args = parser.parse_args()

    data = {}

    # Opening JSON file 
    with open(args.json_file) as json_file:
        ady_json = json.load(json_file)
    
    if args.filter_inhibitory:

        print("{")
        key_values = sorted(ady_json.items())

        for key, values in key_values[:-1]:
            print_row_if_inh(key, values, end=",\n")

        key, values = key_values[-1]
        print_row_if_inh(key, values, end="\n")

        print("}")
        
    else:
        # Print the type of data variable 
        print("Type:", type(data)) 

        for n,v in data["ALNL"]:
            print(n,v)
        # Print the data of dictionary 
        #print("\nPeople1:", data['people1']) 
        #print("\nPeople2:", data['people2']) 


if __name__ == '__main__':
    main()
