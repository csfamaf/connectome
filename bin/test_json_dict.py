import json 
import argparse

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("json_file_list", help="JSON file with list values")
    parser.add_argument("json_file_dict", help="JSON file with dict values")
    args = parser.parse_args()

    ady_json_list = {}
    ady_json_dict = {}

    # Opening JSON file 
    with open(args.json_file_list) as json_file:
        ady_json_list = json.load(json_file)

    with open(args.json_file_dict) as json_file:
        ady_json_dict = json.load(json_file)
    
    if ady_json_list.keys() != ady_json_dict.keys():
        print("keys not equal")
        print(set(ady_json_list.keys()) - set(ady_json_dict.keys()))
        print(set(ady_json_dict.keys()) - set(ady_json_list.keys()))

    for k in ady_json_dict.keys():
        dict_val = [[n, v] for n, v in ady_json_dict[k].items()]
        if dict_val != ady_json_list[k]:
            print(k)
            print(ady_json_list[k])
            print(dict_val)
            print("---------------")


if __name__ == '__main__':
    main()
