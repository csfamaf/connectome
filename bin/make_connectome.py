from util.UpdatedSpreadsheetDataReader import read_data, read_muscle_data
from util.NeuroMLUtilities import ConnectionInfo

from collections import Counter
from collections import defaultdict

from typing import List
import sys, os

begin_prog =  \
    """import copy

class SynapticsEmmons:

    def __init__(self):

        self.postSynaptic = {}

        self.thisState = 0
        self.nextState = 1

        self.createpostSynaptic()

    ###############################################################################
    #                                                                             #
    #                 Listas con los nombres de los musculos                      #
    #                                                                             #
    ###############################################################################

    # Se usa para eliminar disparos de Axon ya que los músculos no pueden disparar.

    # The Threshold is the maximum sccumulated value that must be exceeded before
    # the Neurite will fire
    threshold = 30

    muscles = ['MVU', 'MVL', 'MDL', 'MVR', 'MDR']

    
    muscleList = ['MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15', 'MDL16', 'MDL17',
                  'MDL18', 'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23', 'MVL07', 'MVL08', 'MVL09', 'MVL10', 'MVL11',
                  'MVL12', 'MVL13', 'MVL14', 'MVL15', 'MVL16', 'MVL17', 'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22',
                  'MVL23', 'MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15', 'MDR16',
                  'MDR17', 'MDR18', 'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23', 'MVR07', 'MVR08', 'MVR09', 'MVR10',
                  'MVR11', 'MVR12', 'MVR13', 'MVR14', 'MVR15', 'MVR16', 'MVR17', 'MVR18', 'MVR19', 'MVR20', 'MVL21',
                  'MVR22', 'MVR23']
    
    mLeft = ['MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15', 'MDL16', 'MDL17', 'MDL18',
             'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23', 'MVL07', 'MVL08', 'MVL09', 'MVL10', 'MVL11', 'MVL12', 'MVL13',
             'MVL14', 'MVL15', 'MVL16', 'MVL17', 'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22', 'MVL23']
    mRight = ['MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15', 'MDR16', 'MDR17', 
              'MDR18', 'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23', 'MVR07', 'MVR08', 'MVR09', 'MVR10', 'MVR11', 
              'MVR12', 'MVR13', 'MVR14', 'MVR15', 'MVR16', 'MVR17', 'MVR18', 'MVR19', 'MVR20', 'MVL21', 'MVR22', 
              'MVR23']
    # Used to accumulate muscle weighted values in body muscles 07-23 = worm locomotion
    musDleft = ['MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15', 'MDL16', 'MDL17', 
                'MDL18', 'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23']
    musVleft = ['MVL07', 'MVL08', 'MVL09', 'MVL10', 'MVL11', 'MVL12', 'MVL13', 'MVL14', 'MVL15', 'MVL16', 'MVL17', 
                'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22', 'MVL23']
    musDright = ['MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15', 'MDR16', 'MDR17', 
                 'MDR18', 'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23']
    musVright = ['MVR07', 'MVR08', 'MVR09', 'MVR10', 'MVR11', 'MVR12', 'MVR13', 'MVR14', 'MVR15', 'MVR16', 'MVR17', 
                 'MVR18', 'MVR19', 'MVR20', 'MVL21', 'MVR22', 'MVR23']

    """


end_prog = \
    """
    def dendriteAccumulate(self, dneuron):
        f = getattr(self, dneuron)
        # f = eval(dneuron)
        f()


    def fireNeuron(self, fneuron):
        # The threshold has been exceeded and we fire the neurite
        # print(thisState, self.nextState)

        if fneuron != "MVULVA":
            f = getattr(self, fneuron)
            # f = eval(fneuron)
            f()
            # self.postSynaptic[fneuron][thisState] = 0
            self.postSynaptic[fneuron][self.nextState] = 0


    def fire_neurons(self):
        for ps in self.postSynaptic:
            if ps[:3] not in self.muscles and abs(self.postSynaptic[ps][self.thisState]) > self.threshold:
                self.fireNeuron(ps)


    def stimulate_nose(self):
        self.dendriteAccumulate("FLPR")
        self.dendriteAccumulate("FLPL")
        self.dendriteAccumulate("ASHL")
        self.dendriteAccumulate("ASHR")
        self.dendriteAccumulate("IL1VL")
        self.dendriteAccumulate("IL1VR")
        self.dendriteAccumulate("OLQDL")
        self.dendriteAccumulate("OLQDR")
        self.dendriteAccumulate("OLQVR")
        self.dendriteAccumulate("OLQVL")


    def stimulate_food(self):
        self.dendriteAccumulate("ADFL")
        self.dendriteAccumulate("ADFR")
        self.dendriteAccumulate("ASGR")
        self.dendriteAccumulate("ASGL")
        self.dendriteAccumulate("ASIL")
        self.dendriteAccumulate("ASIR")
        self.dendriteAccumulate("ASJR")
        self.dendriteAccumulate("ASJL")

    def swapStates(self):

        # Copy nextState values to thisState
        for ps in self.postSynaptic:
            # if postSynaptic[ps][thisState] != 0:
            #         print ps
            #         print "Before Clone: ", postSynaptic[ps][thisState]

            # fired neurons keep getting reset to previous weight
            # wtf deepcopy -- So, the concern is that the deepcopy doesnt
            # scale up to larger neural networks??
            self.postSynaptic[ps][self.thisState] = copy.deepcopy(self.postSynaptic[ps][self.nextState])

            # this deep copy is not in the functioning version currently.
            # print "After Clone: ", postSynaptic[ps][thisState]

        # Swap state index
        self.thisState, self.nextState = self.nextState, self.thisState


    def printVars(self):
        print(self.thisState, self.nextState,
              self.postSynaptic['VD7'][self.thisState], self.postSynaptic['VD7'][self.nextState])
              """

# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__

def some_test(nnconns, mconns):

    nnpconns = [(c.pre_cell, c.post_cell) for c in nnconns]
    nnpconns_uniq = list(Counter(nnpconns).keys())

    print("conecciones n->n repetidas:")
    print(len(nnconns), len(nnpconns_uniq))
    # print(len(set(nnpconns)))

    mpconns = [(c.pre_cell, c.post_cell) for c in mconns]
    mpconns_uniq = list(set(mpconns))

    print("conecciones ->m repetidas:")
    print(len(mconns), len(mpconns_uniq))

    print("Entre ellas no se repiten:")
    nnmpconns = nnpconns_uniq + mpconns_uniq
    nnmpconns_unique = list(set(nnmpconns))
    print(len(nnmpconns), len(nnmpconns_unique))


def get_left_muscle_prefixes():
    return ["MDL", "MVL"]


def get_right_muscle_prefixes():
    return ["MDR", "MVR"]


def get_muscle_prefixes():
    return get_left_muscle_prefixes()+get_right_muscle_prefixes()


def is_muscle(cell: str):
    return cell.startswith(tuple(get_muscle_prefixes()))


def is_inhibitory(cls: str):
    return cls.upper() == "GABA"


def main():

    nnconns: List[ConnectionInfo]
    nncells: List[str]
    mconns: List[ConnectionInfo]
    mcells: List[str]

    blockPrint()
    nncells, nnconns = read_data(include_nonconnected_cells=True)
    enablePrint()

    assert(len(nncells) == 302)

    # print("Lengths are equal if include_nonconnected_cells=True")
    #
    # print("Found %s cells: %s..." % (len(nncells), nncells))
    # print("Found %s connections: %s..." % (len(nnconns), nnconns[0]))

    blockPrint()
    mneurons, muscles, mconns = read_muscle_data()
    enablePrint()

    # print("Found %i neurons connected to muscles: %s" % (len(mneurons), sorted(mneurons)))
    # print("Found %i muscles connected to neurons: %s" % (len(mmuscles), sorted(mmuscles)))
    # print("Found %i connections between neurons and muscles, e.g. %s" % (len(mconns), mconns[0]))

    # some_test(nnconns, mconns)

    print(begin_prog)

    # # print muscle list
    # print("\n\n    muscleList = {}".format(muscles))
    #
    # #print left muscle list
    # mLeft = [ m for m in muscles if m.startswith(tuple(get_left_muscle_prefixes()))]
    # mRight = [ m for m in muscles if m.startswith(tuple(get_right_muscle_prefixes()))]
    # print("\n    mLeft = {}".format(mLeft))
    # print("\n    mRight = {}".format(mRight))

    dconnec1 = defaultdict(list)

    for c in nnconns+mconns:
        dconnec1[c.pre_cell].append(c)

    for pre_cell in sorted(dconnec1.keys()):
        print("\n\n    def {}(self):".format(pre_cell))

        dconnec = defaultdict(list)
        for c in dconnec1[pre_cell]:
            if is_muscle(c.post_cell) and is_inhibitory(c.synclass):
                dconnec[c.post_cell].append(-c.number)
            else:
                dconnec[c.post_cell].append(c.number)

        for post_cell in sorted(dconnec.keys()):
            numbers = dconnec[post_cell]
            w = sum(numbers)

            print("        self.postSynaptic['{}'][self.nextState] += {}".format(post_cell, w))

    print("\n    def createpostSynaptic(self):")

    for n in sorted(set([c.post_cell for c in nnconns+mconns])):
        print("        self.postSynaptic['{}'] = [0, 0]".format(n))

    print(end_prog)

    # for c in dconnec1['IL1DL']:
    #     print(c)

    # for c in dconnec1['VD13']:
    #     print(c)

    # for mc in mconns:
    #     print(mc)

    # print(set([(c.syntype, c.synclass) for c in mconns]))
    # for c in [c for c in mconns if c.synclass == "Generic_GJ" and not c.pre_cell.startswith(("MD","MV")) ]:
    #     print(c)

    # for c in [c for c in nnconns if c.synclass == "GABA" ]:
    #     print(c)


if __name__ == '__main__':
    main()
