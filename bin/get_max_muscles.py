#!/usr/bin/python3

postSynaptic = {}
nextState = 0

muscleList = ['MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15', 'MDL16', 'MDL17',
              'MDL18', 'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23', 'MVL07', 'MVL08', 'MVL09', 'MVL10', 'MVL11',
              'MVL12', 'MVL13', 'MVL14', 'MVL15', 'MVL16', 'MVL17', 'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22',
              'MVL23', 'MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15', 'MDR16',
              'MDR17', 'MDR18', 'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23', 'MVR07', 'MVR08', 'MVR09', 'MVR10',
              'MVR11', 'MVR12', 'MVR13', 'MVR14', 'MVR15', 'MVR16', 'MVR17', 'MVR18', 'MVR19', 'MVR20', 'MVL21',
              'MVR22', 'MVR23']

mLeft = ['MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15', 'MDL16', 'MDL17', 'MDL18',
         'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23', 'MVL07', 'MVL08', 'MVL09', 'MVL10', 'MVL11', 'MVL12', 'MVL13',
         'MVL14', 'MVL15', 'MVL16', 'MVL17', 'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22', 'MVL23']

mRight = ['MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15', 'MDR16', 'MDR17', 'MDR18',
          'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23', 'MVR07', 'MVR08', 'MVR09', 'MVR10', 'MVR11', 'MVR12', 'MVR13',
          'MVR14', 'MVR15', 'MVR16', 'MVR17', 'MVR18', 'MVR19', 'MVR20', 'MVL21', 'MVR22', 'MVR23']

def init():

    global postSynaptic
    global muscleList

    for m in muscleList:
        postSynaptic[m] = [0, 0]


def decrement():

    global postSynaptic
    global nextState

    postSynaptic['MDL07'][nextState] += -6
    postSynaptic['MDL08'][nextState] += -6
    postSynaptic['MDL09'][nextState] += -7
    postSynaptic['MDL09'][nextState] += -6
    postSynaptic['MDL10'][nextState] += -6
    postSynaptic['MDL11'][nextState] += -7
    postSynaptic['MDL11'][nextState] += -7
    postSynaptic['MDL12'][nextState] += -6
    postSynaptic['MDL13'][nextState] += -9
    postSynaptic['MDL13'][nextState] += -7
    postSynaptic['MDL14'][nextState] += -7
    postSynaptic['MDL14'][nextState] += -2
    postSynaptic['MDL15'][nextState] += -7
    postSynaptic['MDL16'][nextState] += -7
    postSynaptic['MDL17'][nextState] += -7
    postSynaptic['MDL18'][nextState] += -7
    postSynaptic['MDL19'][nextState] += -7
    postSynaptic['MDL20'][nextState] += -7
    postSynaptic['MDL21'][nextState] += -7
    postSynaptic['MDL21'][nextState] += -3
    postSynaptic['MDL22'][nextState] += -7
    postSynaptic['MDL23'][nextState] += -7
    postSynaptic['MVL07'][nextState] += -7
    postSynaptic['MVL08'][nextState] += -2
    postSynaptic['MVL08'][nextState] += -5
    postSynaptic['MVL09'][nextState] += -7
    postSynaptic['MVL10'][nextState] += -5
    postSynaptic['MVL10'][nextState] += -7
    postSynaptic['MVL11'][nextState] += -9
    postSynaptic['MVL12'][nextState] += -9
    postSynaptic['MVL12'][nextState] += -9
    postSynaptic['MVL13'][nextState] += -7
    postSynaptic['MVL14'][nextState] += -17
    postSynaptic['MVL14'][nextState] += -7
    postSynaptic['MVL15'][nextState] += -7
    postSynaptic['MVL15'][nextState] += -18
    postSynaptic['MVL16'][nextState] += -7
    postSynaptic['MVL16'][nextState] += -7
    postSynaptic['MVL17'][nextState] += -10
    postSynaptic['MVL17'][nextState] += -9
    postSynaptic['MVL18'][nextState] += -10
    postSynaptic['MVL19'][nextState] += -9
    postSynaptic['MVL19'][nextState] += -5
    postSynaptic['MVL20'][nextState] += -9
    postSynaptic['MVL20'][nextState] += -9
    postSynaptic['MVL21'][nextState] += -5
    postSynaptic['MVL21'][nextState] += -9
    postSynaptic['MVL22'][nextState] += -9
    postSynaptic['MVL23'][nextState] += -9
    postSynaptic['MDR07'][nextState] += -6
    postSynaptic['MDR08'][nextState] += -6
    postSynaptic['MDR09'][nextState] += -7
    postSynaptic['MDR09'][nextState] += -6
    postSynaptic['MDR10'][nextState] += -6
    postSynaptic['MDR11'][nextState] += -7
    postSynaptic['MDR11'][nextState] += -7
    postSynaptic['MDR12'][nextState] += -6
    postSynaptic['MDR13'][nextState] += -9
    postSynaptic['MDR13'][nextState] += -7
    postSynaptic['MDR14'][nextState] += -7
    postSynaptic['MDR15'][nextState] += -7
    postSynaptic['MDR16'][nextState] += -7
    postSynaptic['MDR17'][nextState] += -7
    postSynaptic['MDR18'][nextState] += -7
    postSynaptic['MDR19'][nextState] += -7
    postSynaptic['MDR20'][nextState] += -7
    postSynaptic['MDL21'][nextState] += -7
    postSynaptic['MDL21'][nextState] += -3
    postSynaptic['MDR22'][nextState] += -7
    postSynaptic['MDR23'][nextState] += -7
    postSynaptic['MVR07'][nextState] += -7
    postSynaptic['MVR08'][nextState] += -3
    postSynaptic['MVR08'][nextState] += -5
    postSynaptic['MVR09'][nextState] += -7
    postSynaptic['MVR10'][nextState] += -5
    postSynaptic['MVR10'][nextState] += -7
    postSynaptic['MVR11'][nextState] += -9
    postSynaptic['MVR12'][nextState] += -7
    postSynaptic['MVR12'][nextState] += -9
    postSynaptic['MVR13'][nextState] += -7
    postSynaptic['MVR14'][nextState] += -17
    postSynaptic['MVR14'][nextState] += -7
    postSynaptic['MVR15'][nextState] += -7
    postSynaptic['MVR15'][nextState] += -18
    postSynaptic['MVR16'][nextState] += -7
    postSynaptic['MVR16'][nextState] += -7
    postSynaptic['MVR17'][nextState] += -10
    postSynaptic['MVR17'][nextState] += -9
    postSynaptic['MVR18'][nextState] += -10
    postSynaptic['MVR19'][nextState] += -9
    postSynaptic['MVR19'][nextState] += -5
    postSynaptic['MVR20'][nextState] += -9
    postSynaptic['MVR20'][nextState] += -9
    postSynaptic['MVL21'][nextState] += -5
    postSynaptic['MVL21'][nextState] += -9
    postSynaptic['MVR22'][nextState] += -5
    postSynaptic['MVR22'][nextState] += -9
    postSynaptic['MVR23'][nextState] += -9


def increment():

    postSynaptic['MDL07'][nextState] += 3
    postSynaptic['MDL07'][nextState] += 2
    postSynaptic['MDL07'][nextState] += 1
    postSynaptic['MDL07'][nextState] += 1
    postSynaptic['MDL08'][nextState] += 3
    postSynaptic['MDL08'][nextState] += 2
    postSynaptic['MDL08'][nextState] += 8
    postSynaptic['MDL08'][nextState] += 1
    postSynaptic['MDL08'][nextState] += 1
    postSynaptic['MDL08'][nextState] += 1
    postSynaptic['MDL08'][nextState] += 1
    postSynaptic['MDL09'][nextState] += 3
    postSynaptic['MDL09'][nextState] += 2
    postSynaptic['MDL09'][nextState] += 5
    postSynaptic['MDL09'][nextState] += 3
    postSynaptic['MDL10'][nextState] += 3
    postSynaptic['MDL10'][nextState] += 2
    postSynaptic['MDL10'][nextState] += 5
    postSynaptic['MDL10'][nextState] += 3
    postSynaptic['MDL11'][nextState] += 2
    postSynaptic['MDL11'][nextState] += 2
    postSynaptic['MDL11'][nextState] += 4
    postSynaptic['MDL11'][nextState] += 6
    postSynaptic['MDL11'][nextState] += 3
    postSynaptic['MDL11'][nextState] += 3
    postSynaptic['MDL12'][nextState] += 2
    postSynaptic['MDL12'][nextState] += 5
    postSynaptic['MDL12'][nextState] += 4
    postSynaptic['MDL12'][nextState] += 4
    postSynaptic['MDL12'][nextState] += 3
    postSynaptic['MDL12'][nextState] += 3
    postSynaptic['MDL13'][nextState] += 3
    postSynaptic['MDL13'][nextState] += 2
    postSynaptic['MDL13'][nextState] += 5
    postSynaptic['MDL13'][nextState] += 4
    postSynaptic['MDL13'][nextState] += 4
    postSynaptic['MDL13'][nextState] += 2
    postSynaptic['MDL14'][nextState] += 3
    postSynaptic['MDL14'][nextState] += 2
    postSynaptic['MDL14'][nextState] += 5
    postSynaptic['MDL14'][nextState] += 4
    postSynaptic['MDL14'][nextState] += 4
    postSynaptic['MDL14'][nextState] += 3
    postSynaptic['MDL14'][nextState] += 2
    postSynaptic['MDL15'][nextState] += 2
    postSynaptic['MDL15'][nextState] += 4
    postSynaptic['MDL15'][nextState] += 2
    postSynaptic['MDL16'][nextState] += 3
    postSynaptic['MDL16'][nextState] += 4
    postSynaptic['MDL16'][nextState] += 2
    postSynaptic['MDL17'][nextState] += 2
    postSynaptic['MDL17'][nextState] += 4
    postSynaptic['MDL17'][nextState] += 4
    postSynaptic['MDL17'][nextState] += 2
    postSynaptic['MDL17'][nextState] += 2
    postSynaptic['MDL18'][nextState] += 3
    postSynaptic['MDL18'][nextState] += 4
    postSynaptic['MDL18'][nextState] += 2
    postSynaptic['MDL19'][nextState] += 3
    postSynaptic['MDL19'][nextState] += 4
    postSynaptic['MDL19'][nextState] += 4
    postSynaptic['MDL19'][nextState] += 2
    postSynaptic['MDL19'][nextState] += 2
    postSynaptic['MDL20'][nextState] += 3
    postSynaptic['MDL20'][nextState] += 2
    postSynaptic['MDL20'][nextState] += 4
    postSynaptic['MDL20'][nextState] += 2
    postSynaptic['MDL21'][nextState] += 1
    postSynaptic['MDL21'][nextState] += 4
    postSynaptic['MDL21'][nextState] += 2
    postSynaptic['MDL21'][nextState] += 2
    postSynaptic['MDL22'][nextState] += 1
    postSynaptic['MDL22'][nextState] += 4
    postSynaptic['MDL22'][nextState] += 2
    postSynaptic['MDL23'][nextState] += 1
    postSynaptic['MDL23'][nextState] += 4
    postSynaptic['MDL23'][nextState] += 2
    postSynaptic['MVL07'][nextState] += 1
    postSynaptic['MVL07'][nextState] += 3
    postSynaptic['MVL07'][nextState] += 5
    postSynaptic['MVL07'][nextState] += 1
    postSynaptic['MVL07'][nextState] += 4
    postSynaptic['MVL08'][nextState] += 1
    postSynaptic['MVL08'][nextState] += 1
    postSynaptic['MVL08'][nextState] += 3
    postSynaptic['MVL08'][nextState] += 1
    postSynaptic['MVL09'][nextState] += 3
    postSynaptic['MVL09'][nextState] += 5
    postSynaptic['MVL09'][nextState] += 4
    postSynaptic['MVL10'][nextState] += 1
    postSynaptic['MVL10'][nextState] += 5
    postSynaptic['MVL10'][nextState] += 5
    postSynaptic['MVL10'][nextState] += 4
    postSynaptic['MVL11'][nextState] += 1
    postSynaptic['MVL11'][nextState] += 6
    postSynaptic['MVL11'][nextState] += 5
    postSynaptic['MVL11'][nextState] += 6
    postSynaptic['MVL11'][nextState] += 5
    postSynaptic['MVL12'][nextState] += 1
    postSynaptic['MVL12'][nextState] += 1
    postSynaptic['MVL12'][nextState] += 5
    postSynaptic['MVL12'][nextState] += 6
    postSynaptic['MVL12'][nextState] += 4
    postSynaptic['MVL12'][nextState] += 6
    postSynaptic['MVL13'][nextState] += 2
    postSynaptic['MVL13'][nextState] += 5
    postSynaptic['MVL13'][nextState] += 4
    postSynaptic['MVL13'][nextState] += 6
    postSynaptic['MVL14'][nextState] += 2
    postSynaptic['MVL14'][nextState] += 5
    postSynaptic['MVL14'][nextState] += 5
    postSynaptic['MVL14'][nextState] += 6
    postSynaptic['MVL14'][nextState] += 5
    postSynaptic['MVL14'][nextState] += 6
    postSynaptic['MVL15'][nextState] += 4
    postSynaptic['MVL15'][nextState] += 6
    postSynaptic['MVL15'][nextState] += 5
    postSynaptic['MVL15'][nextState] += 6
    postSynaptic['MVL15'][nextState] += 5
    postSynaptic['MVL16'][nextState] += 4
    postSynaptic['MVL16'][nextState] += 6
    postSynaptic['MVL16'][nextState] += 6
    postSynaptic['MVL17'][nextState] += 5
    postSynaptic['MVL17'][nextState] += 5
    postSynaptic['MVL17'][nextState] += 6
    postSynaptic['MVL18'][nextState] += 5
    postSynaptic['MVL18'][nextState] += 5
    postSynaptic['MVL18'][nextState] += 5
    postSynaptic['MVL19'][nextState] += 5
    postSynaptic['MVL19'][nextState] += 5
    postSynaptic['MVL20'][nextState] += 5
    postSynaptic['MVL20'][nextState] += 5
    postSynaptic['MVL20'][nextState] += 6
    postSynaptic['MVL20'][nextState] += 5
    postSynaptic['MVL21'][nextState] += 5
    postSynaptic['MVL21'][nextState] += 5
    postSynaptic['MVL22'][nextState] += 1
    postSynaptic['MVL22'][nextState] += 5
    postSynaptic['MVL22'][nextState] += 5
    postSynaptic['MVL23'][nextState] += 5
    postSynaptic['MVL23'][nextState] += 5
    postSynaptic['MDR07'][nextState] += 3
    postSynaptic['MDR07'][nextState] += 2
    postSynaptic['MDR07'][nextState] += 1
    postSynaptic['MDR07'][nextState] += 1
    postSynaptic['MDR08'][nextState] += 4
    postSynaptic['MDR08'][nextState] += 3
    postSynaptic['MDR08'][nextState] += 8
    postSynaptic['MDR08'][nextState] += 2
    postSynaptic['MDR08'][nextState] += 1
    postSynaptic['MDR08'][nextState] += 2
    postSynaptic['MDR08'][nextState] += 1
    postSynaptic['MDR09'][nextState] += 3
    postSynaptic['MDR09'][nextState] += 2
    postSynaptic['MDR09'][nextState] += 5
    postSynaptic['MDR09'][nextState] += 3
    postSynaptic['MDR10'][nextState] += 3
    postSynaptic['MDR10'][nextState] += 2
    postSynaptic['MDR10'][nextState] += 5
    postSynaptic['MDR10'][nextState] += 3
    postSynaptic['MDR11'][nextState] += 3
    postSynaptic['MDR11'][nextState] += 2
    postSynaptic['MDR11'][nextState] += 4
    postSynaptic['MDR11'][nextState] += 4
    postSynaptic['MDR11'][nextState] += 3
    postSynaptic['MDR11'][nextState] += 3
    postSynaptic['MDR12'][nextState] += 2
    postSynaptic['MDR12'][nextState] += 5
    postSynaptic['MDR12'][nextState] += 4
    postSynaptic['MDR12'][nextState] += 4
    postSynaptic['MDR12'][nextState] += 3
    postSynaptic['MDR12'][nextState] += 3
    postSynaptic['MDR13'][nextState] += 3
    postSynaptic['MDR13'][nextState] += 2
    postSynaptic['MDR13'][nextState] += 5
    postSynaptic['MDR13'][nextState] += 4
    postSynaptic['MDR13'][nextState] += 4
    postSynaptic['MDR13'][nextState] += 2
    postSynaptic['MDR14'][nextState] += 3
    postSynaptic['MDR14'][nextState] += 2
    postSynaptic['MDR14'][nextState] += 5
    postSynaptic['MDR14'][nextState] += 4
    postSynaptic['MDR14'][nextState] += 4
    postSynaptic['MDR14'][nextState] += 3
    postSynaptic['MDR14'][nextState] += 2
    postSynaptic['MDR15'][nextState] += 2
    postSynaptic['MDR15'][nextState] += 4
    postSynaptic['MDR15'][nextState] += 2
    postSynaptic['MDR16'][nextState] += 3
    postSynaptic['MDR16'][nextState] += 4
    postSynaptic['MDR16'][nextState] += 2
    postSynaptic['MDR17'][nextState] += 2
    postSynaptic['MDR17'][nextState] += 4
    postSynaptic['MDR17'][nextState] += 4
    postSynaptic['MDR17'][nextState] += 2
    postSynaptic['MDR17'][nextState] += 2
    postSynaptic['MDR18'][nextState] += 3
    postSynaptic['MDR18'][nextState] += 4
    postSynaptic['MDR18'][nextState] += 2
    postSynaptic['MDR19'][nextState] += 3
    postSynaptic['MDR19'][nextState] += 4
    postSynaptic['MDR19'][nextState] += 4
    postSynaptic['MDR19'][nextState] += 2
    postSynaptic['MDR19'][nextState] += 2
    postSynaptic['MDR20'][nextState] += 3
    postSynaptic['MDR20'][nextState] += 2
    postSynaptic['MDR20'][nextState] += 4
    postSynaptic['MDR20'][nextState] += 2
    postSynaptic['MDL21'][nextState] += 1
    postSynaptic['MDL21'][nextState] += 4
    postSynaptic['MDL21'][nextState] += 2
    postSynaptic['MDL21'][nextState] += 2
    postSynaptic['MDR22'][nextState] += 1
    postSynaptic['MDR22'][nextState] += 4
    postSynaptic['MDR22'][nextState] += 2
    postSynaptic['MDR23'][nextState] += 1
    postSynaptic['MDR23'][nextState] += 4
    postSynaptic['MDR23'][nextState] += 2
    postSynaptic['MVR07'][nextState] += 1
    postSynaptic['MVR07'][nextState] += 1
    postSynaptic['MVR07'][nextState] += 1
    postSynaptic['MVR07'][nextState] += 1
    postSynaptic['MVR07'][nextState] += 3
    postSynaptic['MVR07'][nextState] += 5
    postSynaptic['MVR07'][nextState] += 1
    postSynaptic['MVR07'][nextState] += 4
    postSynaptic['MVR08'][nextState] += 1
    postSynaptic['MVR08'][nextState] += 1
    postSynaptic['MVR08'][nextState] += 3
    postSynaptic['MVR08'][nextState] += 1
    postSynaptic['MVR09'][nextState] += 5
    postSynaptic['MVR09'][nextState] += 4
    postSynaptic['MVR10'][nextState] += 5
    postSynaptic['MVR10'][nextState] += 5
    postSynaptic['MVR10'][nextState] += 4
    postSynaptic['MVR11'][nextState] += 6
    postSynaptic['MVR11'][nextState] += 5
    postSynaptic['MVR11'][nextState] += 6
    postSynaptic['MVR11'][nextState] += 5
    postSynaptic['MVR12'][nextState] += 5
    postSynaptic['MVR12'][nextState] += 6
    postSynaptic['MVR12'][nextState] += 4
    postSynaptic['MVR12'][nextState] += 6
    postSynaptic['MVR13'][nextState] += 5
    postSynaptic['MVR13'][nextState] += 4
    postSynaptic['MVR13'][nextState] += 6
    postSynaptic['MVR14'][nextState] += 2
    postSynaptic['MVR14'][nextState] += 5
    postSynaptic['MVR14'][nextState] += 5
    postSynaptic['MVR14'][nextState] += 6
    postSynaptic['MVR14'][nextState] += 5
    postSynaptic['MVR14'][nextState] += 6
    postSynaptic['MVR15'][nextState] += 4
    postSynaptic['MVR15'][nextState] += 6
    postSynaptic['MVR15'][nextState] += 5
    postSynaptic['MVR15'][nextState] += 6
    postSynaptic['MVR15'][nextState] += 5
    postSynaptic['MVR16'][nextState] += 4
    postSynaptic['MVR16'][nextState] += 6
    postSynaptic['MVR16'][nextState] += 6
    postSynaptic['MVR17'][nextState] += 5
    postSynaptic['MVR17'][nextState] += 5
    postSynaptic['MVR17'][nextState] += 6
    postSynaptic['MVR18'][nextState] += 5
    postSynaptic['MVR18'][nextState] += 5
    postSynaptic['MVR18'][nextState] += 5
    postSynaptic['MVR19'][nextState] += 5
    postSynaptic['MVR19'][nextState] += 5
    postSynaptic['MVR20'][nextState] += 5
    postSynaptic['MVR20'][nextState] += 5
    postSynaptic['MVR20'][nextState] += 6
    postSynaptic['MVR20'][nextState] += 5
    postSynaptic['MVL21'][nextState] += 5
    postSynaptic['MVL21'][nextState] += 5
    postSynaptic['MVR22'][nextState] += 5
    postSynaptic['MVR22'][nextState] += 5
    postSynaptic['MVR23'][nextState] += 5
    postSynaptic['MVR23'][nextState] += 5


# def print_syns(nom_var):
#
#     for m in muscleList:
#         print("self.{}[{}] = {}".format(nom_var, m, postSynaptic[m][nextState]))

def sum_synaptics(place):

    s = 0
    for m in place:
        s += postSynaptic[m][nextState]

    return s

def main():
    init()
    decrement()
    print("Min accumleft =", sum_synaptics(mLeft))
    print("Min accumright =", sum_synaptics(mRight))
    print()

    init()
    increment()
    print("Max accumleft =", sum_synaptics(mLeft))
    print("Max accumright =", sum_synaptics(mRight))
    print()


if __name__ == '__main__':
    main()
